# PSTerraformEnv

A very simple Terraform version manager similar to [`tfenv`](https://github.com/tfutils/tfenv) and [`tfswitch`](https://github.com/warrensbox/terraform-switcher) but purely in PowerShell. It doesn't contain any code from those two projects however, it does use some of the same environment variables. It also uses a similar directory structure for the versions to `tfenv`.

## Goals

I had a need to swap between multiple version of Terraform. I looked around at my option. I found`tfenv` and then `tfswitch` but neither were available as native packages for the Linux distro I was using. So I went to look at their code repos. Both struck me as much more complex than I really needed, so I decided to just implement the functionality in PowerShell.

**MY MAIN GOALS FOR THIS MODULE**

* Be able to install, and swap between different versions of `terraform` based on whats in a `.terraform-version` file
* A very simple code (hopefully less than a couple hundred lines)
* Minimal dependencies

## Supported Operating Systems

An attempt to support Linux, Mac, and Windows is being made but it has not been tested on anything other than Linux.

* Linux
* macOS (maybe)
* Windows (Maybe)

## Getting Started

This module is on [PowerShellGallary as PSTerraformEnv](https://www.powershellgallery.com/packages/PSTerraformEnv/). You can install it by entering the following command into your PowerShell session.

```
Install-Module PSTerraformEnv
```

Alternatibly, you can also clone this repo, into one of the directories in your `$env:PSModulePath`. This is most useful if you'd like to develop the module itself.

```
git clone git@gitlab.com:druonysus/psterraformenv.git $HOME/.local/share/powershell/Modules/PSTerraformEnv
```